#include "TrackTruthLLPDecorator.hh"

#include "TruthTools.hh"


float get_dr(const xAOD::TruthVertex* vertex_A, const xAOD::TruthVertex* vertex_B) {
  if ( !vertex_A or !vertex_B ) {
    return NAN;
  }
  float dr = (vertex_A->v4() - vertex_B->v4()).Vect().Mag(); 
  return dr;
}


bool TrackTruthLLPDecorator::sort_tracks(const xAOD::TrackParticle* track_A , const xAOD::TrackParticle* track_B ) {
  return track_A->pt() < track_B->pt();
}

// the constructor just builds the decorator
TrackTruthLLPDecorator::TrackTruthLLPDecorator(const std::string& prefix,
                                         const std::string& tool_pfx,
                                         const std::vector<int>& pdgIdList):
  m_track_origin_label(prefix + "truthOriginLabel"),
  m_track_production_vertex_idx(prefix + "truthVertexIndex"),
  m_track_truth_barcode(prefix + "truthBarcode"),
  m_track_parent_barcode(prefix + "truthParentBarcode"),
  m_InDetTrackTruthOriginTool(tool_pfx + "InDetTrackTruthOriginTool"),
  m_pdgIdList(pdgIdList)
{
  if (!m_InDetTrackTruthOriginTool.initialize()) {
    throw std::runtime_error("Can't initialize InDetTrackTruthOriginTool");
  }
}

const xAOD::TruthVertex* TrackTruthLLPDecorator::get_truth_vertex( const xAOD::TrackParticle* track ) const {
  
  // get the truth particle
  const xAOD::TruthParticle* truth = m_InDetTrackTruthOriginTool.getTruth(track);

  // no truth
  if ( not truth ) {
    return nullptr;
  }

  // no vertex
  const xAOD::TruthVertex* truth_vertex = truth->prodVtx();
  if ( not truth_vertex or truth_vertex->perp() > 440.0 ) { 
    return nullptr;
  }

  return truth_vertex;
}


const xAOD::TruthVertex* TrackTruthLLPDecorator::get_nearest_vertex(const xAOD::TruthVertex* search_vertex, 
                                                                 std::vector<const xAOD::TruthVertex*> vertices) const {

  if ( !search_vertex ) {
    return nullptr;
  }

  float min_dr = INFINITY;
  int min_dr_idx = -1;
  int index = -1;

  // find closest vertex
  for ( auto vertex : vertices ) {
    index++;
    if ( !vertex or search_vertex == vertex ) {
      continue;
    }
    float dr = get_dr(vertex, search_vertex);
    if ( dr < min_dr ) {
      min_dr = dr;
      min_dr_idx = index;
    }
  }

  // return closest vertex
  if ( min_dr_idx < 0 ) {
    return nullptr;
  } else {
    return vertices.at(min_dr_idx);
  }
}


// this call actually does the work on the track
void TrackTruthLLPDecorator::decorateAll(TrackSelector::Tracks tracks,
                                      const xAOD::TruthVertex* truth_PV) const {

  // sort the tracks by pt to ensure the vertex clustering is deterministic
  std::sort(tracks.begin(), tracks.end(), sort_tracks);

  // store truth vertex for each track
  auto trk_truth_vertex = std::vector<const xAOD::TruthVertex*>();
  for ( const auto& track : tracks ) {

    // store the truth origin of the track
    int trackTruthOrigin = m_InDetTrackTruthOriginTool.getTrackOrigin(track);
    // get exclusive track origin as defined here:
    // https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/PhysicsAnalysis/TrackingID/InDetTrackSystematicsTools/InDetTrackSystematicsTools/InDetTrackTruthOriginDefs.h#0137
    int trackTruthLabel = InDet::ExclusiveOrigin::getExclusiveOrigin(trackTruthOrigin);

    m_track_origin_label(*track) = ExclusiveOriginLLP::Unknown;

    switch (trackTruthLabel) {
    case InDet::ExclusiveOrigin::Pileup:
        m_track_origin_label(*track) = ExclusiveOriginLLP::Pileup;
        break;
    case InDet::ExclusiveOrigin::Fake:
        m_track_origin_label(*track) = ExclusiveOriginLLP::Fake;
        break;

    case InDet::ExclusiveOrigin::Primary:
        m_track_origin_label(*track) = ExclusiveOriginLLP::Primary;
        break;

    case InDet::ExclusiveOrigin::FromB:
    case InDet::ExclusiveOrigin::FromBC:
    case InDet::ExclusiveOrigin::FromC:
        m_track_origin_label(*track) = ExclusiveOriginLLP::FromHF;
        break;

    case InDet::ExclusiveOrigin::FromTau:
        m_track_origin_label(*track) = ExclusiveOriginLLP::FromTau;
        break;

    case InDet::ExclusiveOrigin::OtherSecondary:
        m_track_origin_label(*track) = ExclusiveOriginLLP::OtherSecondary;
        break;
    }
    
    auto truth = m_InDetTrackTruthOriginTool.getTruth(track);
    // store the truth barcode and check if from LLP
    m_track_truth_barcode(*track) = -2;
    if (truth) {
      if(checkProduction(*truth, 0) != -1) {
          m_track_origin_label(*track) = ExclusiveOriginLLP::FromLLP;
      }
       if(checkProduction(*truth, 0) != -2) {
	 std::cerr << "Exceeded max recursion depth, you have checked over 100 generations. Problem in truth record?" << std::endl;
       }
      m_track_truth_barcode(*track) = truth->barcode();
    }

    // store the parent barcode
    int parent_barcode = -2;
    if ( m_track_origin_label(*track) == ExclusiveOriginLLP::FromHF) {
      auto parent_hadron = truth::getParentHadron(truth);
      if ( parent_hadron ) {
        parent_barcode = parent_hadron->barcode();
      }
    }
    m_track_parent_barcode(*track) = parent_barcode;
    
    // get the truth vertex of the track and store for now
    auto truth_vertex = get_truth_vertex(track);
    trk_truth_vertex.push_back(truth_vertex);
  }

  // decorate tracks with truth vertex info
  auto seen_vertices = std::vector<const xAOD::TruthVertex*>();
  for ( size_t i = 0; i != trk_truth_vertex.size(); i++) {
    auto this_vert  = trk_truth_vertex.at(i);
    auto this_track = tracks.at(i);

    // do we have a vertex for this track?
    if ( !this_vert ) {
      m_track_production_vertex_idx(*this_track) = -2;
      continue;
    }    

    // track from PV
    if ( get_dr(this_vert, truth_PV) < min_dr_to_merge ) {
      m_track_production_vertex_idx(*this_track) = 0;
      continue;
    }

    // have we already seen this vertex?
    bool new_vertex = true;
    for ( size_t j = 0; j != seen_vertices.size(); j++) {
      float dr = get_dr(seen_vertices.at(j), this_vert);

      if ( dr < min_dr_to_merge ) {
        // a vertex is nearby, reuse it
        new_vertex = false;
        m_track_production_vertex_idx(*this_track) = j+1;
        break;
      }
    }

    // this vertex is far enough away from others to be considered unique
    if ( new_vertex ) {
      seen_vertices.push_back(this_vert);
      m_track_production_vertex_idx(*this_track) = seen_vertices.size();
    }
  }
}


// check if truth particle originated from decay of particle in the pdgIdList
int TrackTruthLLPDecorator::checkProduction( const xAOD::TruthParticle & truthPart , int counter) const {
  if (truthPart.nParents() == 0){
    return -1;
  }

  else if (counter > 100) {
    return -2;
  }

  else{
    const xAOD::TruthParticle * parent = truthPart.parent(0);
    if(not parent) {
      return -1;
    }
    if(std::find(m_pdgIdList.begin(), m_pdgIdList.end(), std::abs(parent->pdgId())) != m_pdgIdList.end()) {
        const xAOD::TruthVertex* vertex = parent->decayVtx();
        return vertex->barcode();
    }
    // recurse on parent
    return checkProduction(*parent, counter+1);
  }
  return -1;
}
