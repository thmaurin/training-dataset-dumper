#include "HitdRMinDecoratorAlg.h"

#include "StoreGate/WriteDecorHandle.h"
#include "StoreGate/ReadDecorHandle.h"

#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "MCTruthClassifier/IMCTruthClassifier.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "AthLinks/ElementLink.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/Vertex.h"
#include <math.h>

#include "TrackSelector.hh"
#include <TVector3.h>

struct EtaPhi {
  float eta;
  float phi;
};

HitdRMinDecoratorAlg::HitdRMinDecoratorAlg(
    const std::string &name, ISvcLocator *loc) : AthReentrantAlgorithm(name, loc)
{
}

StatusCode HitdRMinDecoratorAlg::initialize()
{
  ATH_MSG_DEBUG("Inizializing " << name() << "... ");
  ATH_CHECK(m_tracks.initialize());
  ATH_CHECK(m_JetPixelCluster.initialize());
  ATH_CHECK(m_JetSCTCluster.initialize());
  ATH_CHECK(m_Vertex.initialize());

  ATH_CHECK(m_sctDr.initialize());
  ATH_CHECK(m_pixDr.initialize());

  return StatusCode::SUCCESS;
}

StatusCode HitdRMinDecoratorAlg::execute(const EventContext &ctx) const
{
  ATH_MSG_DEBUG("Executing " << name() << "... ");

  SG::ReadHandle<xAOD::TrackParticleContainer> full_tracks{m_tracks, ctx};
  SG::ReadHandle<TMVC> JetPixelCluster{m_JetPixelCluster, ctx};
  SG::ReadHandle<TMVC> JetSCTCluster{m_JetSCTCluster, ctx};
  SG::ReadHandle<xAOD::VertexContainer>   Vertices{m_Vertex, ctx};

  if (!Vertices.isValid())
  {
    ATH_MSG_ERROR("Couldn't find Vertices");
    return StatusCode::FAILURE;
  }
  if (!JetPixelCluster.isValid())
  {
    ATH_MSG_ERROR("Couldn't find Pixel");
    return StatusCode::FAILURE;
  }
  if (!JetSCTCluster.isValid())
  {
    ATH_MSG_ERROR("Couldn't find SCT");
    return StatusCode::FAILURE;
  }
  if (!full_tracks.isValid())
  {
    ATH_MSG_ERROR("Couldn't find tracks");
    return StatusCode::FAILURE;
  }

  int count = 0;
  TVector3 vx3(0,0,0);

  for (auto vx : *Vertices) {
    if ( vx->vertexType() == xAOD::VxType::PriVtx ) {
      if (count >= 1) {
        ATH_MSG_ERROR("More than one Primary Vertex");
        return StatusCode::FAILURE;
      }
      vx3.SetX(vx->x());
      vx3.SetY(vx->y());
      vx3.SetZ(vx->z());

      count++;
    }
  }

  std::vector<EtaPhi> tracks;
  for (const auto& track: *full_tracks) {
    EtaPhi tr;
    tr.eta = track->eta();
    tr.phi = track->phi();
    tracks.push_back(tr);
  }

  for ( const auto& key: {m_sctDr, m_pixDr}) {
    SG::WriteDecorHandle<TMVC, float> hitDec{key, ctx};
    for (const auto& hit_full: *hitDec) {

      TVector3 v_hit(
        hit_full->globalX(),
        hit_full->globalY(),
        hit_full->globalZ());
      TVector3 hit = v_hit-vx3;

      float Phi_hit = hit.Phi();
      float Eta_hit = hit.Eta();
      float dR_2 = INFINITY;
      for (const auto& track : tracks) {
        float deta = Eta_hit - track.eta;
        float dphi = Phi_hit - track.phi;

        if (dphi > M_PI) {
          dphi -= M_PI*2;
        } else if (dphi <= -M_PI) {
          dphi += M_PI*2;
        }

        float tmp_dR_2 = dphi*dphi+deta*deta;
        if (tmp_dR_2 < dR_2)
        {
          dR_2 = tmp_dR_2;
        }
      }
      hitDec(*hit_full) = std::sqrt(dR_2);
    }
  }
  return StatusCode::SUCCESS;
}
