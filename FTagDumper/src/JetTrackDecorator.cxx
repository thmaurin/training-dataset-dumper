#include "JetTrackDecorator.hh"
#include "xAODJet/Jet.h"

#include "InDetTrackSystematicsTools/InDetTrackTruthOriginDefs.h"

// the constructor just builds the decorator
JetTrackDecorator::JetTrackDecorator(const std::string& suffix,
				     const std::string& writer_name)
  : m_ftagTruthOriginLabel("ftagTruthOriginLabel"+suffix),
    m_n_tracks("n_"+writer_name),
    m_n_b_tracks("n_b_"+writer_name),
    m_n_c_tracks("n_c_"+writer_name),
    m_n_bc_tracks("n_bc_"+writer_name),
    m_n_hf_tracks("n_hf_"+writer_name),
    m_n_tau_tracks("n_tau_"+writer_name),
    m_n_pv_tracks("n_pv_"+writer_name),
    m_n_othersec_tracks("n_othersec_"+writer_name),
    m_n_pu_tracks("n_pu_"+writer_name),
    m_n_fake_tracks("n_fake_"+writer_name),
    m_PU_track_fraction("PU_fraction_"+writer_name) {}

// this call actually does the work on the jet
void JetTrackDecorator::decorate
(const xAOD::Jet& jet,
 const std::vector<const xAOD::TrackParticle*>& tracks) const {

  int n_pu_tracks = 0;
  int n_fake_tracks = 0;
  int n_b_tracks = 0;
  int n_bc_tracks = 0;
  int n_c_tracks = 0;
  int n_pv_tracks = 0;
  int n_tau_tracks = 0;
  int n_other_sec_tracks = 0;
  for(const auto& track : tracks){
    int origin = m_ftagTruthOriginLabel.withDefault(*track, -1);
    switch(origin){
      case InDet::ExclusiveOrigin::Pileup:
        n_pu_tracks++;
        break;
      case InDet::ExclusiveOrigin::Fake:
        n_fake_tracks++;
        break;
      case InDet::ExclusiveOrigin::Primary:
        n_pv_tracks++;
        break;
      case InDet::ExclusiveOrigin::FromB:
        n_b_tracks++;
        break;
      case InDet::ExclusiveOrigin::FromBC:
        n_bc_tracks++;
        break;
      case InDet::ExclusiveOrigin::FromC:
        n_c_tracks++;
        break;
      case InDet::ExclusiveOrigin::FromTau:
        n_tau_tracks++;
        break;
      case InDet::ExclusiveOrigin::OtherSecondary:
        n_other_sec_tracks++;
        break;
      case -1:
      case -2:
        break;
      default:
        throw std::runtime_error("Unknown ftagTruthOrigin label " + std::to_string(origin));
    }
  }
  int n_hf_tracks = n_b_tracks + n_bc_tracks + n_c_tracks;

  m_n_tracks(jet) = tracks.size();
  m_n_b_tracks(jet) = n_b_tracks;
  m_n_c_tracks(jet) = n_c_tracks;
  m_n_bc_tracks(jet) = n_bc_tracks;
  m_n_hf_tracks(jet) = n_hf_tracks;
  m_n_tau_tracks(jet) = n_tau_tracks;
  m_n_pv_tracks(jet) = n_pv_tracks;
  m_n_othersec_tracks(jet) = n_other_sec_tracks;
  m_n_pu_tracks(jet) = n_pu_tracks;
  m_n_fake_tracks(jet) = n_fake_tracks;


  m_PU_track_fraction(jet) = static_cast<float>(n_pu_tracks + n_fake_tracks)/tracks.size();

}
