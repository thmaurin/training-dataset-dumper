#ifndef ASSOCIATION_CONFIG_H
#define ASSOCIATION_CONFIG_H

#include <string>

namespace jetwriters {

  template <typename T>
  struct AssociationConfig
  {
    T accessor;
    std::string name;
  };

  template <typename T>
  AssociationConfig<T> getAssociationConfig(
    T accessor,
    const std::string& name)
  {
    return AssociationConfig<T> {accessor, name};
  }

}

#endif
